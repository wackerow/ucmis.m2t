[comment encoding = UTF-8 /]
[module commonRdf('http://www.eclipse.org/uml2/5.0.0/Types', 'http://www.eclipse.org/uml2/5.0.0/UML', 'http://www.eclipse.org/uml2/5.0.0/UML/Profile/Standard')]

[import ucmis::m2t::query::modelQuery /]
[import ucmis::m2t::service::utilityServices /]

[template public ttl_prefixes(x : NamedElement)]
[comment][template public ttl_prefixes(x: OclAny)][/comment]
PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl:   <http://www.w3.org/2002/07/owl#>
PREFIX xsd:   <http://www.w3.org/2001/XMLSchema#>
PREFIX dc:    <http://purl.org/dc/elements/1.1/>
PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
PREFIX cdi:   <[getProperty('DDI-CDI_RDF_NS')/]>
PREFIX ucmis: <tag:ddialliance.org,2024:ucmis:>
[/template]

[**
 * Compute the IRI for a given element.
 * NB: I was not able to retrieve the xmi:uuid, so I rebuild an IRI here.
 * This may need to be adapted.
 * 
 * TODO: maybe this should be a query rather than a template? (no need to post-process...
 * 
 * Current version uses short id
 * Previous versions are iri1 (the xmi:id, not xmi:uuid which does not seem to be retrievable) and
 * iri2. 
 */]
[template public iri(x : NamedElement) post(cdiAdHoc()) ]
[if x.ancestors()->notEmpty()]
	[if ( x.oclIsTypeOf(Property) ) ]
		[x.eContainer().oclAsType(Classifier).name/]-[x.name/]
	[else]
	    [x.name /]
	[/if]
[else]
	[x.builtinIri()/]
[/if]
[/template]

[template public iri1(x : OclAny) post(cdiAdHoc())]
[if x.ancestors()->notEmpty()]
  [x.getXmiId()/]
[else][
  x.builtinIri()
/][/if]
[/template]

[template public iri2(x : OclAny) post(cdiAdHoc())]
[if x.ancestors()->notEmpty()]
  [let aSize: Integer = x.ancestors()->size()]
    m:[x.ancestors()->reverse()->subSequence(2, aSize)->append(x)->collect(x: OclAny | x.eGet('name'))->sep('-')/]
  [/let]
[else][
  x.builtinIri()
/][/if]
[/template]

[**
 * Creates a local fragment IRI named after x.
 */]
[template public fragment(x : NamedElement) post(noWhitespace()) ]
<#[if x.ancestors()->notEmpty()]
	[if ( x.oclIsTypeOf(Property) ) ]
		[x.eContainer().oclAsType(Classifier).name/]-[x.name/]
	[else]
	    [x.name /]
	[/if]
[else]
	[x.builtinIri()/]
[/if]>
[/template]

[**
 * Creates a local fragment IRI named after x.
 */]
[template public fragmentRev(x : NamedElement) post(noWhitespace()) ]
<#rev_[if x.ancestors()->notEmpty()]
	[if ( x.oclIsTypeOf(Property) ) ]
		[x.eContainer().oclAsType(Classifier).name/]-[x.name/]
	[else]
	    [x.name /]
	[/if]
[else]
	[x.builtinIri()/]
[/if]>
[/template]

[**
 * Return the IRI of a builtin primitive datatype
 * TODO it would probably be better to fail with an error message here
*/]
[query public builtinIri(x : OclAny) : String =
	x.toString().substring(x.toString().index('#')+1).substitute( ')', '' )
/]

[query public builtinIri2(x : OclAny) : String =
  let typeName: String = x.toString().substring(x.toString().index('#')+1).substitute( ')', '' )
  in (
    if typeName = 'String' then 'xsd:string'
    else if typeName = 'Boolean' then 'xsd:boolean'
    else if typeName = 'Integer' then 'xsd:integer'
    else if typeName = 'Real' then 'xsd:double'
    else if typeName = 'UnlimitedNatural' then 'xsd:nonNegativeInteger'
    else 'm:DEBUG-BUITIN-TYPE:'.concat(typeName)
    endif endif endif endif endif
  )
/]
[comment]    else if typeName = 'UnlimitedNatural' then 'xsd:double'[/comment]


[template public rdfs_comment(comments : Set(Comment))]
[if comments->notEmpty()]rdfs:comment "[comments._body.trim().ttlEscape()/]"@[comments.getModel().e_modelLanguage()/];[/if][/template]


[template public noWhitespace(txt: String)]
[txt.replaceAll('\\s+', '')/]
[/template]

[**
 * Ensure that the IRI is valid and Turtle friendly.
 * 
 * TODO: at the moment, the 'valid' part is not really checked,
 * and the 'turtle friendly' part is managed in a very ad-hoc way
*/]
[template public iriEscape(txt: String) post(noWhitespace())]
[let trimmed: String = txt.trim()]
[if trimmed.endsWith('.')][trimmed/]-[else][trimmed/][/if]
[/let]
[/template]


[**
 * TODO As its name imply, this method is specific to CDI.
 * Ultimately, what it does should be generically supported by using UML traces,
 * then it must be removed and its uses replaced with iriEscape.
*/]
[template public cdiAdHoc(txt: String) post(noWhitespace())]
[let iri: String = txt.iriEscape()]
	[if (e_mappedCdiItemsRdf()->indexOf(iri)).oclIsUndefined()]
		cdi:[iri/]
	[else]
		[e_mappedCdiItemsRdfTargets()->at(e_mappedCdiItemsRdf()->indexOf(iri))/]
	[/if]
[/let]
[/template]

[template public cdiAdHoc2(txt: String) post(noWhitespace())]
[let iri: String = txt.iriEscape()]
  [if iri.endsWith(':XsdAnyUri')] xsd:anyURI
  [elseif iri.endsWith(':XsdDate')]xsd:date
  [elseif iri.endsWith(':XsdLanguage')]xsd:language
  [elseif iri.endsWith(':LanguageString')]rdf:langString
  [elseif iri.endsWith(':CatalogDetails-creator')]dc:creator
[comment]  [if iri.endsWith('-XsdAnyUri')] xsd:anyURI
  [elseif iri.endsWith('-XsdDate')]xsd:date
  [elseif iri.endsWith('-XsdLanguage')]xsd:language[/comment]
  [else][iri/]
  [/if]
[/let]
[/template]


[template public ttlEscape(txt: String)]
[txt.replaceAll('\\\\', '\\\\\\\\')
    .replaceAll('\\n', '\\\\n')
    .replaceAll('"', '\\\\"')
/]
[/template]


[query public shortName(anAssociation : Association) : String =
	if getProperty('ddi-cdi_ambiguous_assoc_names').substituteAll(' ', '').tokenize(',')->asSet()->count(anAssociation.name.tokenize('_')->at(2)) = 0 
	then anAssociation.name.tokenize('_')->at(2)
	else anAssociation.name.tokenize('_')->at(2).concat('_').concat(anAssociation.name.tokenize('_')->at(3))
	endif
/]

[query public source(anAssociation : Association) : Type =
	anAssociation.ownedEnd->first().type
/]

[query public target(anAssociation : Association) : Type =
	anAssociation.ownedEnd->first().getOtherEnd().type
/]

[query public isComplex(aType : Type) : Boolean =
	not aType.oclIsTypeOf(Enumeration) and not aType.oclIsTypeOf(PrimitiveType)
/]

[query public isInAllowedCdiPackage(aNamedElement : NamedElement) : Boolean =
	not ( getProperty('ddi-cdi_packages_ignore').substituteAll(' ', '').tokenize(',')
		->exists(ignoredPackage | aNamedElement.qualifiedName.contains('::' + ignoredPackage) )
	)
/]

[query public isNotInMappedItems(aClassifier : Classifier) : Boolean =
	e_mappedCdiItemsRdf()->indexOf(aClassifier.name).oclIsUndefined()
/]
